fetch('getNoOfMatchesPlayed.json')
    .then(
        function(response) {
            if (response.status !== 200) {
                console.log('Looks like there was a problem. Status Code: ' +
                    response.status);
                return;
            }

            // Examine the text in the response
            response.json().then(function(data) {
                var match = [];
                // Object.keys(data).forEach((keys)=>{
                //     let x = {};
                //     x["name"]=keys;
                //     let m = [];
                //     Object.keys(data[keys]).forEach( (key)=>{
                //         m.push(data[keys][key])
                //     } )
                //     x["data"]=m;
                //     match.push(x);
                // })

                var chart = Highcharts.chart('container', {

                    title: {
                        text: 'getNoOfMatchesPlayed'
                    },

                    subtitle: {
                        text: 'Years'
                    },

                    xAxis: {
                        categories: Object.keys(data)
                    },
                    yAxis: {
                        title: {
                            text: "Matches"
                        }
                    },

                    series: [{
                        type: 'column',
                        colorByPoint: true,
                        data: Object.values(data),
                        showInLegend: false
                    }]

                });


                $('#plain').click(function() {
                    chart.update({
                        chart: {
                            inverted: false,
                            polar: false
                        },
                        subtitle: {
                            text: 'Plain'
                        }
                    });
                });

                $('#inverted').click(function() {
                    chart.update({
                        chart: {
                            inverted: true,
                            polar: false
                        },
                        subtitle: {
                            text: 'Inverted'
                        }
                    });
                });

                $('#polar').click(function() {
                    chart.update({
                        chart: {
                            inverted: false,
                            polar: true
                        },
                        subtitle: {
                            text: 'Polar'
                        }
                    });
                });
            });
        }
    )
    .catch(function(err) {
        console.log('Fetch Error :-S', err);
    });



fetch('getEconomicalBowlersForYear.json')
    .then(
        function(response) {

            if (response.status !== 200) {
                console.log('Looks like there was a problem. Status Code: ' +
                    response.status);
                return;
            }

            // Examine the text in the response
            response.json().then(function(data) {
                var chart = Highcharts.chart('container2', {

                    title: {
                        text: 'getEconomicalBowlersForYear'
                    },

                    subtitle: {
                        text: 'Players'
                    },

                    xAxis: {
                        categories: Object.keys(data)
                    },
                    yAxis: {
                        title: {
                            text: "Runs they gave"
                        }
                    },
                    // yAxis:{
                    //   title:'Number Of matches'
                    // },

                    series: [{
                        type: 'column',
                        colorByPoint: true,
                        data: Object.values(data),
                        showInLegend: false
                    }]

                });


                $('#plain').click(function() {
                    chart.update({
                        chart: {
                            inverted: false,
                            polar: false
                        },
                        subtitle: {
                            text: 'Plain'
                        }
                    });
                });

                $('#inverted').click(function() {
                    chart.update({
                        chart: {
                            inverted: true,
                            polar: false
                        },
                        subtitle: {
                            text: 'Inverted'
                        }
                    });
                });

                $('#polar').click(function() {
                    chart.update({
                        chart: {
                            inverted: false,
                            polar: true
                        },
                        subtitle: {
                            text: 'Polar'
                        }
                    });
                });
            });
        }
    )
    .catch(function(err) {
        console.log('Fetch Error :-S', err);
    });




fetch('getExtraRunsPerTeamForYear.json')
    .then(
        function(response) {

            if (response.status !== 200) {
                console.log('Looks like there was a problem. Status Code: ' +
                    response.status);
                return;
            }

            // Examine the text in the response
            response.json().then(function(data) {
                var chart = Highcharts.chart('container3', {
                    title: {
                        text: 'getExtraRunsPerTeamForYear'
                    },

                    subtitle: {
                        text: 'Teams'
                    },

                    xAxis: {
                        categories: Object.keys(data)
                    },
                    yAxis: {
                        title: {
                            text: "Extra Runs"
                        }
                    },
                    // yAxis:{
                    //   title:'Number Of matches'
                    // },

                    series: [{
                        type: 'column',
                        colorByPoint: true,
                        data: Object.values(data),
                        showInLegend: false
                    }]
                });


                $('#plain').click(function() {
                    chart.update({
                        chart: {
                            inverted: false,
                            polar: false
                        },
                        subtitle: {
                            text: 'Plain'
                        }
                    });
                });

                $('#inverted').click(function() {
                    chart.update({
                        chart: {
                            inverted: true,
                            polar: false
                        },
                        subtitle: {
                            text: 'Inverted'
                        }
                    });
                });

                $('#polar').click(function() {
                    chart.update({
                        chart: {
                            inverted: false,
                            polar: true
                        },
                        subtitle: {
                            text: 'Polar'
                        }
                    });
                });
            });
        }
    )
    .catch(function(err) {
        console.log('Fetch Error :-S', err);
    });




fetch('getNoOfMatchesWonPerTeamPerYear.json')
    .then(
        function(response) {
            if (response.status !== 200) {
                console.log('Looks like there was a problem. Status Code: ' +
                    response.status);
                return;
            }

            // Examine the text in the response
            response.json().then(function(data) {


                let teams = data["teams"];
                let years = data["years"];
                let fdata = data["ans"];

                let plot = [];
                console.log(data)
                for (let i = 0; i < teams.length; i++) {
                    let x = {};
                    x["name"] = teams[i];
                    let y = [];
                    for (let j = 0; j < years.length; j++) {
                        if (fdata[years[j]][teams[i]]) {
                            y.push(fdata[years[j]][teams[i]])
                        } else {
                            if (teams[i] !== null)
                                y.push(0);
                        }
                    }
                    x["data"] = y;
                    plot.push(x);

                }
                console.log(plot)
                console.log(years)
                // console.log(Object.keys(plot))


                let match = [];
                // Object.keys(data).forEach((keys)=>{

                //             let x = {};
                //             x["name"]=keys;
                //             let m = [];
                //             Object.keys(data[keys]).forEach( (key)=>{
                //                 m.push(data[keys][key])
                //             } )
                //             x["data"]=m;
                //             match.push(x);
                //       })

                Highcharts.chart('container4', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'getNoOfMatchesWonPerTeamPerYear'
                    },
                    subtitle: {
                        text: 'Source: MountBlue'
                    },
                    xAxis: {
                        categories: years,
                        crosshair: true
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Matches Won Per year'
                        }
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                            '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
                        footerFormat: '</table>',
                        shared: true,
                        useHTML: true
                    },
                    plotOptions: {
                        column: {
                            pointPadding: 0.2,
                            borderWidth: 0
                        }
                    },
                    series: plot
                });
            })

        })

    .catch(function(err) {
        console.log('Fetch Error :-S', err);
    });