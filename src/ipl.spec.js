import {
  getNoOfMatchesPlayed,
  getNoOfMatchesWonPerTeamPerYear,
  getExtraRunsPerTeamForYear,
  getEconomicalBowlersForYear
} from "./ipl";

describe("IPL module", () => {
  describe("No. of matches played per team for all years, getNoOfMatchesPlayed", () => {
    const matchesSample = [
      {
        season: '2008'
      },
      {
        season: '2009'
      },
      {
        season: '2008'
      }
    ];
    const expectedResult = {
  '2008': 58,
  '2009': 57,
  '2010': 60,
  '2011': 73,
  '2012': 74,
  '2013': 76,
  '2014': 60,
  '2015': 59,
  '2016': 60,
  '2017': 59,
  'average':63.6  
    };
    test("should exist", () => {
      expect(getNoOfMatchesPlayed).toBeDefined();
    });
    test("should return an object", () => {
      expect(getNoOfMatchesPlayed(matchesSample)).toBeDefined();
      expect(typeof getNoOfMatchesPlayed(matchesSample)).toEqual("object");
      expect(getNoOfMatchesPlayed(matchesSample)).toEqual(expectedResult);
    });
  });

  describe("No. of matches won per team per year, getNoOfMatchesWonPerTeamPerYear", () => 
  {


    test("should exist", () => {
      expect(getNoOfMatchesWonPerTeamPerYear).toBeDefined();
    });
    test("should return an object",()=>{
      expect(typeof getNoOfMatchesWonPerTeamPerYear).toEqual("object");
    } )




  });
  describe("Extra runs conceeded per team for year, getExtraRunsPerTeamForYear", () => {
    const year = {
      "year":'2016'
    };
    const result = {
      'Mumbai Indians': 102,
  'Rising Pune Supergiants': 101,
  'Delhi Daredevils': 109,
  'Kolkata Knight Riders': 130,
  'Kings XI Punjab': 83,
  'Gujarat Lions': 132,
  'Royal Challengers Bangalore': 118,
  'Sunrisers Hyderabad': 124
    };
    test("should exist", () => {
      expect(getExtraRunsPerTeamForYear).toBeDefined();
    });
    test("expectedResult do not  match",()=>{
      expect(getExtraRunsPerTeamForYear(year)).toBeDefined();
      expect(typeof getExtraRunsPerTeamForYear(year)).toEqual("object");
      expect(getExtraRunsPerTeamForYear(year)).toEqual(result);
      ;
    })
  });


  describe("Economical bowlers for year, getEconomicalBowlersForYear", () => {
    const year = {
      "year":'2015'
    };
    const result = {
      
  'RN ten Doeschate': 4,
  'J Yadav': 4.142857142857143,
  'V Kohli': 5,
  'R Ashwin': 5.871794871794871,
  'S Nadeem': 6.142857142857143,
  'Parvez Rasool': 6.2,
  'Z Khan': 6.36,
  'MC Henriques': 6.56,
  'M Vijay': 7,
  'GB Hogg': 7.0476190476190474 
    };

    test("should exist", () => {
      expect(getEconomicalBowlersForYear).toBeDefined();
    });
    test("expect result do not match",()=>{
      expect(getExtraRunsPerTeamForYear(year)).toBeDefined();
      expect(typeof getEconomicalBowlersForYear(year)).toEqual("object");
      expect(getEconomicalBowlersForYear(year)).toEqual(result);
    })


  });
});
